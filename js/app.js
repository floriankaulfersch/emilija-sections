let sidebar;
let sidebarList;
let sidebarItems = [];
let sections;
let sectionHeaders;
let backToTopButton;

let timer;

const init = () => {
  sidebarList = document.querySelector('.sidebar-list');
  sidebar = document.querySelector('.sidebar');
  sections = document.querySelectorAll('.section');
  sectionHeaders = document.querySelectorAll('.section-header');
  backToTopButton = document.querySelector('.backToTop');

  createSidebarItems();
  initializeSidebarScrollListener();
  addSectionClickListeners();
  initBackToTopButton()
  createSectionScrollListener();
  addSidebarItemClickListener();
}

const createSidebarItems = () => {
  sections.forEach(section => {
    const name = section.getElementsByClassName('section-title')[0].textContent;
    const el = document.createElement('li');
    el.className = 'sidebar-item';
    el.innerHTML = createSidebarLink(name);
    sidebarItems.push(el);
    sidebarList.appendChild(el);
  })
}

const createSidebarLink = name => `
  <a href="#" class="sidebar-link">${name}</a>
`

const initializeSidebarScrollListener = () => {
  window.addEventListener('scroll', (e) => {
    sidebar.classList.remove('closed');
    if (timer !== null) {
      clearTimeout(timer);
    }
    timer = setTimeout(() => {
      sidebar.classList.add('closed')
    }, 2000);
  });
}

const addSectionClickListeners = () => {
  sectionHeaders.forEach(header => {
    header.addEventListener('click', toggleSection);
  })
}

const toggleSection = e => {
  const parent = e.currentTarget.parentNode;
  parent.classList.toggle('closed');
}

const initBackToTopButton = () => {
  backToTopButton.addEventListener('click', () => {
    window.scrollTo(0,0);
  });

  window.addEventListener('scroll', () => {
    if (window.scrollY > 200){
      backToTopButton.classList.remove('hidden');
    } else {
      backToTopButton.classList.add('hidden');
    }
  })
}

const createSectionScrollListener = () => {
  window.addEventListener('scroll', () => {
    
    sections.forEach((section, i) => {

      const boundingRectangle = section.getBoundingClientRect();
      
      if (boundingRectangle.top >= 0 && boundingRectangle.bottom < window.innerHeight) {
        sidebarItems[i].classList.add('active')
        section.classList.add('active')
      } else {
        sidebarItems[i].classList.remove('active')
        section.classList.remove('active')
      }
    });
  })
}

const addSidebarItemClickListener = () => {
 sidebarItems.forEach((item, i) => {
    item.addEventListener('click', (e) => {
      e.preventDefault();
      sections[i].scrollIntoView({behavior: "smooth"});
    });
  })
}

window.onload = init;

